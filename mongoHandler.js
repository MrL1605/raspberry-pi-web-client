
var MongoClient = require('mongodb').MongoClient;
var DB_NAME = "test";
var connectionURL = "mongodb://localhost:27017/exampleDb";


function insertDoc(data, callback){

    // Connect to the db
    MongoClient.connect(connectionURL, function(err, db) {

        var resp_text = "";
        if(err) {
            resp_text += "Problem with connection to Mongo - " + err;
            console.error(resp_text);
            callback(resp_text, true);
            return;
        }

        console.log("We are connected to Mongo\n");
        resp_text += ("We are connected to Mongo\n");

        // Use second arg as {strict : true} to throw error if collection already exists
        db.createCollection(DB_NAME, function(err, collection) {
            if(err) {
                resp_text += "Problem with creation of Collection - " + err;
                console.error(resp_text);
                callback(resp_text, true);
                return;
            }

            console.log("Connection Created\n");
            resp_text += "Connection Created\n";

            // Use {w:1} to get Error response. Else without callback and {w:1}
            collection.insert(data, {w:1}, function(err, result){
                if (err){
                    resp_text +=  "Problem with insertion of document - " + err;
                    console.error(resp_text);
                    callback(resp_text, true);
                    return;
                }
                console.log("Document is Inserted\n");
                resp_text += "The Document is inserted.\n";
                resp_text += "Got Result \n" + JSON.stringify(result);
                callback(resp_text, false);
            });
        });
    });
}

function getAllDocs(callback){

    // Connect to the db
    MongoClient.connect(connectionURL, function(err, db) {

        var resp_text = [];

        if(err) {
            console.error("Problem with connection to Mongo - " + err);
            resp_text.push("Problem with connection to Mongo - " + err);
            callback(JSON.stringify(resp_text), true);
            return;
        }

        var collection = db.collection(DB_NAME);
        // Or use toArray but that could lead to memory issues, if ResultSet is too large.
        var stream = collection.find().stream();

        stream.on("data", function(item) {
            resp_text.push(item);
        });

        stream.on("end", function() {
            callback(resp_text, false);
        });
    });
}

exports.insertDoc = insertDoc;
exports.getAllDocs = getAllDocs;

