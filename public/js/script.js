/**
 * Created by Lalit on 1/4/17.
 */

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(startPolling);
var chart = new google.visualization.ScatterChart(document.getElementById('gChartContainer'));

var options = {
    title: 'Age vs. Weight comparison',
    hAxis: {title: 'Age', minValue: 0, maxValue: 15},
    vAxis: {title: 'Weight', minValue: 0, maxValue: 15},
    legend: 'none'
};


function httpGetAsync(theUrl, callback){

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    };
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}


function startPolling(){

    setInterval(function(){
        httpGetAsync("http://localhost:18080/getData", function(resp_text){
            resp_text = JSON.parse(resp_text);
            var data = google.visualization.arrayToDataTable(resp_text);
            chart.draw(data, options);
        });
    }, 3000);
}




