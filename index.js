/**
 * Created by Lalit on 30/3/17.
 */

var server = require("./server");
var router =  require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {};
handle["/"] = requestHandlers.indexHTML;
handle["/getData"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;

server.start(router.route, handle);

