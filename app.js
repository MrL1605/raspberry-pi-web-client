/**
 * Created by Lalit on 30/3/17.
 */

var express = require('express');
var app = express();
var hbs = require('hbs');

app.set('port', (process.env.PORT || 18081));

app.use(express.static(__dirname + '/public'));

// Views is directory for all template files
app.set('view engine', 'html');
app.engine('html', hbs.__express);


// Routes are declared here
app.get('/', function(request, response) {
	response.sendFile('./views/pages/index.html');
});

app.post('/', function(request, response) {
	response.sendFile('./views/pages/index.html');
});




// Listener to given port
app.listen(app.get('port'), function() {
  console.log('Server running on - ', app.get('port'));
});
