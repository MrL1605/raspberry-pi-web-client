/**
 * Created by Lalit on 30/3/17.
 */


function route(handle, pathName, response, payload) {

    if (typeof handle[pathName] === 'function') {
        handle[pathName](response, payload);
    } else {
        console.log("404 found to " + pathName);
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not found");
        response.end();
    }
}

exports.route = route;
