/**
 * Created by Lalit on 30/3/17.
 */

var querystring = require("querystring");
var connector = require("./mongoHandler");
var fs = require("fs");

function start(response, payload){

    connector.getAllDocs(function(resp, err){
        if (err){
            console.error("In Error field\n");
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(resp);
            response.end();
        }else{
            console.log("In non error\n");
            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write(JSON.stringify(resp));
            response.end();
        }
    });
}

function upload(response, payload){

	try{
		payload = JSON.parse(payload);
		var raspId = payload["raspId"];
		var data = payload["data"];
        if (raspId != undefined && data != undefined){
            connector.insertDoc(payload, function (resp, err) {
                if (err) {
                    response.writeHead(500, {"Content-Type": "text/plain"});
                    response.write(resp);
                    response.end();
                } else {
                    response.writeHead(200, {"Content-Type": "text/plain"});
                    response.write(resp);
                    response.end();
                }
            });
        }else{
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write("Wrong format for data");
            response.end();
        }
	}catch(e) {
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write("MalFormed Input Data " + payload);
        response.end();
    }
}

function index(response, payload){

    response.writeHead(200, {ContentType : "text/html"});
    fs.readFile('./index.html', function (err, data) {
        if (err) throw err;
        response.write(data);
        response.end();
    });

}

exports.indexHTML = index;
exports.start = start;
exports.upload = upload;
