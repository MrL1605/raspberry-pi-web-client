/**
 * Created by Lalit on 30/3/17.
 */

var http = require("http");
var url = require("url");

function start(route, handle) {

	function onRequest(request, response) {
		
		var pathName = url.parse(request.url).pathname;
		var postData = "";
		request.setEncoding("UTF-8");
		request.addListener("data", function(postDataChunk){
			postData += postDataChunk;
		});

		request.addListener("end", function(){
			route(handle, pathName, response, postData);
		});
	}

	http.createServer(onRequest).listen(18180);
	console.log("Server has started at 18180");
}

exports.start = start;

